rooms: 

db.rooms.insert({	
		name: "single",
		accomadates: 2,
		price: 1000,
		descript: "A simple room with all the basic necessities",
		roomAvailable: 10,
		isAvailable: false
	})

db.rooms.insertMany([
	{
		name: "double1",
		accomadates: 3,
		price: 2000,
		descript: "A room fit for a small family going on a vacation",
		roomAvailable: 5,
		isAvailable: false
	},
	{
		name: "queen",
		accomadates: 4,
		price: 4000,
		descript: "A room with a queen sized bed perfect for a simple getaway",
		roomAvailable: 5,
		isAvailable: false
	}
])

db.rooms.find({name: "double"})

db.rooms.updateMany(
	{_id : ObjectId("6287806a309a1169b3697ea7")},
	{
		$set:{
			roomAvailable: 0
		}

	}
	)